# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FormationItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # define the fields for your item here like:
    # name = scrapy.Field()
    urlannonces = scrapy.Field()
    titre = scrapy.Field()
    urls = scrapy.Field()
    ville = scrapy.Field()
    prix = scrapy.Field()
    addresse = scrapy.Field()
    ref = scrapy.Field()
    IdAnnonce = scrapy.Field()
    IdAgence = scrapy.Field()
    Reference = scrapy.Field()
    tel = scrapy.Field()
    TelAgence = scrapy.Field()
    NomAgence = scrapy.Field()
    AddresseAgence = scrapy.Field()
    taille = scrapy.Field()
    CodePostal = scrapy.Field()
    proxy = scrapy.Field()
    urlagence = scrapy.Field()
    url = scrapy.Field()
    pass
    
class DatabloggerScraperItem(scrapy.Item):
    # The source URL
    url_from = scrapy.Field()
    # The destination URL
    url_to = scrapy.Field()
    